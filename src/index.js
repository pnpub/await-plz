const parseJson = response => response.json().then(json => (json));
const parsePlainText = response => response.text().then(text => (text));

const plz = (promise) => {
  return promise.then(data => {
      if (typeof Response !== 'undefined') {
        if (data instanceof Response && !data.ok) {
          throw ({ status: data.status, data });
        }
        if (data instanceof Response && data.ok) {
          const parseFunction = data.headers.get('Content-Type') === 'text/plain' ? parsePlainText : parseJson;
          return parseFunction(data).then(res => [null, res]);
        }
      }
      return [null, data];
  })
  .catch(err => [err]);
}

module.exports = plz;