![await pls](https://s3-eu-west-1.amazonaws.com/pinchos-statics/await-plz/await-plz2.0.gif "Await pls")

# Are you sick and tired of Trying and Catching

## GET AWAIT-PLZ TODAY

Add the dependency:
```bash
yarn add await-plz
```

Import the script:
```javascript
import plz from 'await-plz';
```

---

`await-plz` is a simple library used to wrap your promises before sending them to be awaited. By utilizing some simple then-catch logic, it returns an array with two values: `[err, response]`. This makes catching errors alot leaner:

#### Before
```javascript
  let someVariable;
  try {
    someVariable = await somePromiseToGetVariable();
  } catch (error) {
    console.error('Failed to get some variable, error:');
    console.error(error);
  }
```

#### After
```javascript
  const [err, response] = await plz(somePromiseToGetVariable);
  if (err) console.error('Failed to get some variable, error:', err);
  if (!response) return console.log('No variable found');
  
  // Do what you wanna do!
```

It even handles `Response` type of reponses (via for example `fetch`)!